package ldh.maker.vo;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ldh123 on 2018/5/26.
 */
public class TableNo {

    private Integer id;
    private String dbName;
    private String content;
    private Integer treeNodeId;

    public List<String> getTableNoes() {
        if (StringUtils.isEmpty(content)) return new ArrayList<>();
        Gson gson = new Gson();
        return gson.fromJson(content, new TypeToken<List<String>>(){}.getType());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getTreeNodeId() {
        return treeNodeId;
    }

    public void setTreeNodeId(Integer treeNodeId) {
        this.treeNodeId = treeNodeId;
    }
}
