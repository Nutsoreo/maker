package ldh.maker;

import ldh.maker.component.MobileContentUiFactory;
import ldh.maker.component.NodejsWebContentUiFactory;
import ldh.maker.util.UiUtil;

/**
 * Created by ldh123 on 2018/8/1.
 */
public class NodejsWebMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setContentUiFactory(new NodejsWebContentUiFactory());
        UiUtil.setType("nodejs");
    }

    protected String getTitle() {
        return "Nodejs Web服务端--代码自动生成工具";
    }

    public static void main(String[] args) throws Exception {
        startDb(null);
        launch(args);
    }
}
