<#--公共头部-->
<#macro header title keywords="" description="">
    <!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width; initial-scale=1.0; shrink-to-fit=no; maximum-scale=1.0;" />
        <meta name="format-detection" content="telephone=no" />
        <title>${title}</title>
        <meta name="keywords" content="${keywords}" />
        <meta name="description" content="${description}" />

        <link href="/webjars/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
        <link href="/webjars/font-awesome/5.11.2/css/all.min.css" rel="stylesheet">
        <link href="/resource/common/css/dashboard.css" rel="stylesheet">
        <#nested>

    </head>
<body>
</#macro>

<#macro body>
    <#include "/common/head.ftl"/>

    <div class="container-fluid">
        <div class="row">

            <#include "/common/left.ftl"/>
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <#nested>
            </main>
        </div>
    </div>
</#macro>
<#--公共底部-->
<#macro footer>
    <script src="/webjars/jquery/3.0.0/jquery.min.js"></script>
    <script src="/webjars/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="/webjars/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>

    <#nested>
</body>
</html>
</#macro>