<#macro FormItem name label>
    <div class="form-group row">
        <label for="${name}" class="col-sm-2 col-form-label">${label}</label>
        <div class="col-sm-10">
            <#nested>
        </div>
    </div>
</#macro>