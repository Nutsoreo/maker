package ldh.maker;

import ldh.maker.component.JavafxClientContentUiFactory;
import ldh.maker.util.UiUtil;

public class DeskMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setContentUiFactory(new JavafxClientContentUiFactory());
        UiUtil.setType("desk");
    }

    protected String getTitle() {
        return "智能代码生成器之生成javafx client代码";
    }

    public static void main(String[] args) throws Exception {
        startDb(null);
        launch(args);
    }
}

