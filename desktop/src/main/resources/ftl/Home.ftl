<?xml version="1.0" encoding="UTF-8"?>

<?import javafx.scene.shape.*?>
<?import javafx.scene.canvas.*?>
<?import javafx.geometry.*?>
<?import javafx.scene.control.*?>
<?import java.lang.*?>
<?import javafx.scene.layout.*?>
<?import org.controlsfx.control.StatusBar?>

<?import ldh.fx.component.LNavPane?>
<VBox xmlns="http://javafx.com/javafx/8" xmlns:fx="http://javafx.com/fxml/1"
            stylesheets="@/css/Home.css" fx:controller="${projectPackage}.controller.HomeController">
    <children>
        <fx:include source="HomeHead.fxml" fx:id="head"/>
        <HBox VBox.vgrow="ALWAYS">
            <LNavPane fx:id="leftPane" styleClass="leftBackPane">
                <content>
                    <fx:include source="HomeLeft.fxml" fx:id="left"/>
                </content>
            </LNavPane>

            <VBox fx:id="contentPane" HBox.hgrow="ALWAYS">
                <children>
                    <VBox>
                        <children>
                            <Label text="Welcome"/>
                        </children>
                    </VBox>
                </children>
            </VBox>
        </HBox>
        <StatusBar styleClass="bottom"/>
    </children>
</VBox>
