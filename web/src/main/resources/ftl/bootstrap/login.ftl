<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib  prefix="Template" tagdir="/WEB-INF/tags" %>

<Template:simple>
    <jsp:attribute name="headContent">
        <link href="${r'${pageContext.request.contextPath}'}/resource/common/css/signin.css" rel="stylesheet">
    </jsp:attribute>
    <jsp:body>
        <div class="container">

            <form class="form-signin" action="/login" method="get">
                <h2 class="form-signin-heading">Please sign in</h2>
                <label for="inputEmail" class="sr-only">Email address</label>
                <input type="email" id="inputEmail" class="form-control" placeholder="Email address" value="demo@demo.com" required autofocus>
                <label for="inputPassword" class="sr-only">Password</label>
                <input type="password" id="inputPassword" class="form-control" value="demo" placeholder="Password" required>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me"> Remember me
                    </label>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>

        </div>
    </jsp:body>
</Template:simple>
