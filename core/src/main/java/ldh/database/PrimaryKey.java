package ldh.database;

import ldh.maker.util.FreeMakerUtil;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class PrimaryKey {

	private Set<Column> columns;

	//主键名称
	private String keyName;
	
	private Column column;
	
	//是否是复合主键
	private boolean isComposite = false;
	
	public PrimaryKey(List<Column> columnAlls, Set<String> columnNames) {
		isComposite = columnNames.size() == 1 ? false : true;
		columns = new HashSet<Column>();
		for (String columnName : columnNames) {
			for (Column c : columnAlls) {
				if (columnName.equals(c.getName())) {
					columns.add(c);
				}
			}
		}
		initName();
	}
	
	public Set<Column> getColumns() {
		return columns;
	}
	
	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}
	
	public String getKeyName() {
		initName();
		return keyName;
	}

	private void initName() {
		if (!this.isComposite) {
			column = columns.iterator().next();
			keyName = column.getProperty();
		} else {
			StringBuilder sb = new StringBuilder();
			boolean isfirst = true;
			for (Column c : columns) {
				if (isfirst) {
					sb.append(c.getProperty());
					isfirst = false;
				} else {
					sb.append(FreeMakerUtil.firstUpper(c.getProperty()));
				}
				
			}
			keyName = sb.toString();
		}
	}

	public boolean isComposite() {
		return isComposite;
	}

	public Column getColumn() {
		return column;
	}

	public void setColumn(Column column) {
		this.column = column;
	}
	
	
	
}
